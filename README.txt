CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Selection Sharer module provides a Medium like popover menu to share on
Twitter or by email any text selected on the page.

 * For a full description of the module visit:
   https://www.drupal.org/project/selection_sharer

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/selection_sharer

 * To contribute the original Javascript project, submit bug reports and
   feature suggestions, or to track changes visit:
   https://github.com/xdamman/selection-sharer

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Selection Sharer module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > System > Selection Sharer to configure.
    3. Enter in the jQuery selectors.
    4. Save configuration.
    5. Now when text is highlighted a pop-up will appear with the choice to
       share via Twitter or email.


MAINTAINERS
-----------

 * Pedro Pelaez (psf_) - https://www.drupal.org/u/psf_

Supporting organization:

 * SDOS - https://www.drupal.org/sdos
